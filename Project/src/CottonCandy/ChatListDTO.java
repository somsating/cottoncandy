package CottonCandy;

public class ChatListDTO {
	private String roomName;
	private int roomNo;
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public int getRoomNo() {
		return roomNo;
	}
	public void setRoomNo(int roomNo) {
		this.roomNo = roomNo;
	}
	@Override
	public String toString() {
		return "ChatListDTO [roomName=" + roomName + ", roomNo=" + roomNo + "]";
	}
	public ChatListDTO() {
		super();
	}
	public ChatListDTO(int roomNo, String roomName) {
		super();
		this.roomNo = roomNo;
		this.roomName = roomName;
	}
}
