package CottonCandy;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Admin extends JFrame {

	private JPanel contentPane;
	private String id="", nick="";
	private JLabel lblLogin;
	private JButton btnWaiting;
	private JButton btnAdmin;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Admin frame = new Admin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Admin(String id, String nick){
		this();
		this.id=id;
		this.nick=nick;
		shownick();
	}
	
	public Admin() {
		setTitle("관리자 로그인");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 240, 100);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lblLogin = new JLabel("");
		lblLogin.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		lblLogin.setBounds(12, 10, 200, 15);
		contentPane.add(lblLogin);
		
		btnWaiting = new JButton("대기실");
		btnWaiting.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(Admin.this, "<html><span style=\"font-family: 맑은 고딕;\">"+nick + "님 환영합니다.</span></html>");
				ChatList chatForm = new ChatList(id);
				chatForm.setVisible(true);
				chatForm.setLocation(100, 100);
				dispose();
			}       
		});
		btnWaiting.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		btnWaiting.setBounds(25, 35, 80, 23);
		contentPane.add(btnWaiting);
		
		btnAdmin = new JButton("관리");
		btnAdmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog(Admin.this, "<html><span style=\"font-family: 맑은 고딕;\">관리자 모드로 로그인합니다.</span></html>");
				LIST listForm = new LIST();
				listForm.setVisible(true);
				listForm.setLocation(100, 100);
				dispose();
			}
		});
		btnAdmin.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		btnAdmin.setBounds(120, 35, 80, 23);
		contentPane.add(btnAdmin);
	}
	public void shownick(){
		lblLogin.setText("관리자 "+nick+"님 환영합니다.");
	}
}
