package CottonCandy;

import java.awt.event.FocusEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import javax.swing.JOptionPane;

public class DAO {
	// -------------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	// ---------------SEARCH-----------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	public Vector searchidx(String idx) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from chatMember where idx like ? order by idx");

			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + idx + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getString("id"));
				row.add(rs.getString("pwd"));
				row.add(rs.getString("nick"));
				row.add(rs.getInt("nickchange"));
				row.add(rs.getString("name"));
				row.add(rs.getDate("birth"));
				row.add(rs.getInt("Sex"));
				row.add(rs.getString("email"));
				row.add(rs.getString("tel"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}

	public Vector searchid(String id) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from chatMember where id like ? order by id");

			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + id + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getString("id"));
				row.add(rs.getString("pwd"));
				row.add(rs.getString("nick"));
				row.add(rs.getInt("nickchange"));
				row.add(rs.getString("name"));
				row.add(rs.getDate("birth"));
				row.add(rs.getInt("Sex"));
				row.add(rs.getString("email"));
				row.add(rs.getString("tel"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}

	public Vector searchnick(String nick) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from chatMember where nick like ? order by nick");

			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + nick + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getString("id"));
				row.add(rs.getString("pwd"));
				row.add(rs.getString("nick"));
				row.add(rs.getInt("nickchange"));
				row.add(rs.getString("name"));
				row.add(rs.getDate("birth"));
				row.add(rs.getInt("Sex"));
				row.add(rs.getString("email"));
				row.add(rs.getString("tel"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}

	public Vector searchtel(String tel) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from chatMember where tel like ? order by tel");

			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + tel + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getString("id"));
				row.add(rs.getString("pwd"));
				row.add(rs.getString("nick"));
				row.add(rs.getInt("nickchange"));
				row.add(rs.getString("name"));
				row.add(rs.getDate("birth"));
				row.add(rs.getInt("Sex"));
				row.add(rs.getString("email"));
				row.add(rs.getString("tel"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}

	public Vector searchbirth(String birth) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from chatMember where birth like ? order by birth");

			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + birth + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getString("id"));
				row.add(rs.getString("pwd"));
				row.add(rs.getString("nick"));
				row.add(rs.getInt("nickchange"));
				row.add(rs.getString("name"));
				row.add(rs.getDate("birth"));
				row.add(rs.getInt("Sex"));
				row.add(rs.getString("email"));
				row.add(rs.getString("tel"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}

	public Vector searchemail(String email) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from chatMember where email like ? order by email");

			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + email + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getString("id"));
				row.add(rs.getString("pwd"));
				row.add(rs.getString("nick"));
				row.add(rs.getInt("nickchange"));
				row.add(rs.getString("name"));
				row.add(rs.getDate("birth"));
				row.add(rs.getInt("Sex"));
				row.add(rs.getString("email"));
				row.add(rs.getString("tel"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}

	// -------------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	// ---------------SEARCH-----------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	public int insert(DTO dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.chatConn();
			String sql = "insert into chatMember values (?,?,?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, dto.getIdx());
			pstmt.setString(2, dto.getId());
			pstmt.setString(3, dto.getPwd());
			pstmt.setString(4, dto.getNick());
			pstmt.setString(6, dto.getName());
			pstmt.setDate(7, dto.getBirth());
			pstmt.setInt(8, dto.getSex());
			pstmt.setString(9, dto.getEmail());
			pstmt.setString(10, dto.getTel());
			result = pstmt.executeUpdate();
			System.out.println(sql);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;

	}

	public int a() {
		String a = "a";
		return 0;

	}

	public int updatechatMember(DTO dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.chatConn();
			String sql = "update chatMember "
					+ " set id=?,pwd=?,nick=?,nickchange=?,name=?,birth=?,sex=?,email=?,tel=? " + " where idx=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(10, dto.getIdx());
			pstmt.setString(1, dto.getId());
			pstmt.setString(2, dto.getPwd());
			pstmt.setString(3, dto.getNick());
			pstmt.setInt(4, dto.getNickchange());
			pstmt.setString(5, dto.getName());
			pstmt.setDate(6, dto.getBirth());
			pstmt.setInt(7, dto.getSex());
			pstmt.setString(8, dto.getEmail());
			pstmt.setString(9, dto.getTel());
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	public Vector listchatMember() {
		// TODO Auto-generated method stub
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			String sql = "select * from chatMember";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getString("id"));
				row.add(rs.getString("pwd"));
				row.add(rs.getString("nick"));
				row.add(rs.getInt("nickchange"));
				row.add(rs.getString("name"));
				row.add(rs.getDate("birth"));
				row.add(rs.getInt("sex"));
				row.add(rs.getString("email"));
				row.add(rs.getString("tel"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;
	}

	// public Vector searchchatMember(String idx) {
	// Vector items = new Vector();
	// Connection conn = null;
	// PreparedStatement pstmt = null;
	// ResultSet rs = null;
	// try {
	// conn = DB.dbConn();
	// StringBuilder sb = new StringBuilder();
	// sb.append("select * from chatMember where idx like ? order by idx");
	// pstmt = conn.prepareStatement(sb.toString());
	// pstmt.setString(1, "%" + idx + "%");
	// rs = pstmt.executeQuery();
	// while (rs.next()) {
	// Vector row = new Vector();
	// row.add(rs.getString("id"));
	// row.add(rs.getString("pwd"));
	// row.add(rs.getString("nick"));
	// row.add(rs.getInt("nickchange"));
	// row.add(rs.getString("name"));
	// row.add(rs.getDate("birth"));
	// row.add(rs.getInt("sex"));
	// row.add(rs.getString("email"));
	// row.add(rs.getString("tel"));
	// items.add(row);
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// } finally {
	// try {
	// if (rs != null)
	// rs.close();
	// } catch (SQLException e) {
	// e.printStackTrace();
	// }
	// try {
	// if (pstmt != null)
	// pstmt.close();
	// } catch (SQLException e) {
	// e.printStackTrace();
	// }
	// try {
	// if (conn != null)
	// conn.close();
	// } catch (SQLException e) {
	// e.printStackTrace();
	// }
	// }
	// return items;
	// }

	public int deletechatMember(String idx) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.chatConn();
			String sql = "delete from chatMember where idx=?";
			// StringBuilder sb = new StringBuilder();
			// sb.append("delete from chatMember where idx=?");
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, idx);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------ID
	// Duplicate--------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public int CheckId(String id) {
		int idx = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DB.chatConn();
			String sql = "select idx from chatMember where id=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				idx = rs.getInt("idx");
			}
		} catch (Exception e) {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		return idx;
	}

	public int CheckNick(String nick) {
		int idx = 0;

		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			conn = DB.chatConn();
			String sql = "select idx from chatMember where nick=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, nick);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				idx = rs.getInt("idx");
			}
		} catch (Exception e) {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		return idx;

	}

	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ---------------------------ID
	// Duplicate---------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

}
