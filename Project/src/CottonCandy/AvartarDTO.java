package CottonCandy;

public class AvartarDTO {
	
	private int avartarno;
	private int skin;
	private int eyes;
	private int hair;
	private int cloth;
	private int acc;
	
	public int getAvartarno() {
		return avartarno;
	}
	public void setAvartarno(int avartarno) {
		this.avartarno = avartarno;
	}
	public int getSkin() {
		return skin;
	}
	public void setSkin(int skin) {
		this.skin = skin;
	}
	public int getEyes() {
		return eyes;
	}
	public void setEyes(int eyes) {
		this.eyes = eyes;
	}
	public int getHair() {
		return hair;
	}
	public void setHair(int hair) {
		this.hair = hair;
	}
	public int getCloth() {
		return cloth;
	}
	public void setCloth(int cloth) {
		this.cloth = cloth;
	}
	public int getAcc() {
		return acc;
	}
	public void setAcc(int acc) {
		this.acc = acc;
	}
	
	@Override
	public String toString() {
		return "AvartarDTO [avartarno=" + avartarno + ", skin=" + skin + ", eyes=" + eyes + ", hair=" + hair
				+ ", cloth=" + cloth + ", acc=" + acc + "]";
	}
	public AvartarDTO(int avartarno, int skin, int eyes, int hair, int cloth, int acc) {
		
		this.avartarno = avartarno;
		this.skin = skin;
		this.eyes = eyes;
		this.hair = hair;
		this.cloth = cloth;
		this.acc = acc;
	}
	
	
	
}
