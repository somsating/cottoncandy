package CottonCandy;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class ChatList extends JFrame implements Runnable {

	private JPanel contentPane;
	private JButton btnEnter;
	private JButton btnCreate;

	// 변수추가
	private ChatListDAO dao;
	private Vector cols;
	private JTable table;
	private JScrollPane scrollPane;
	private JPanel panel_2;
	private String id;
	
	private DefaultTableModel model;
	private JLabel label_2;
	private JLabel label_4;
	private JLabel label_6;
	private JLabel label_8;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChatList frame = new ChatList();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ChatList(String id){
		this();
		this.id=id;
		showPlayer();
	}
	
	
	public ChatList() {

		dao = new ChatListDAO();
		cols = new Vector();
		// cols.add("방 번호");
		cols.add("방 제목");
		// cols.add("방 인원");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 670, 310);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		panel_1.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "채 팅 방", TitledBorder.CENTER,
				TitledBorder.TOP, null, null));
		panel_1.setBounds(168, 46, 477, 215);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
				scrollPane = new JScrollPane();
				scrollPane.setBounds(6, 18, 465, 146);
				panel_1.add(scrollPane);
				
						table = new JTable();
						table.setBackground(Color.WHITE);
						table.setOpaque(true);
						scrollPane.setViewportView(table);
						scrollPane.getViewport().setBackground(table.getBackground());

		panel_2 = new JPanel();
		panel_2.setBounds(6, 176, 465, 27);
		panel_1.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 2, 0, 0));

		btnCreate = new JButton("방 만들기");
		panel_2.add(btnCreate);

		btnEnter = new JButton("방 입장하기");
		panel_2.add(btnEnter);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		panel.setLayout(null);
		panel.setBounds(0, 30, 156, 221);
		contentPane.add(panel);
		
		JLabel label = new JLabel("");
		label.setBounds(28, 0, 100, 100);
		panel.add(label);
		
		JLabel label_1 = new JLabel("ID : ");
		label_1.setHorizontalAlignment(SwingConstants.CENTER);
		label_1.setBounds(12, 110, 45, 15);
		panel.add(label_1);
		
		label_2 = new JLabel("");
		label_2.setBounds(58, 110, 86, 15);
		panel.add(label_2);
		
		JLabel label_3 = new JLabel("NICK : ");
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setBounds(12, 135, 45, 15);
		panel.add(label_3);
		
		label_4 = new JLabel("");
		label_4.setBounds(58, 135, 86, 15);
		panel.add(label_4);
		
		JLabel label_5 = new JLabel("성별 :");
		label_5.setHorizontalAlignment(SwingConstants.CENTER);
		label_5.setBounds(12, 160, 45, 15);
		panel.add(label_5);
		
		label_6 = new JLabel("");
		label_6.setBounds(58, 160, 86, 15);
		panel.add(label_6);
		
		JLabel label_7 = new JLabel("나이 :");
		label_7.setHorizontalAlignment(SwingConstants.CENTER);
		label_7.setBounds(12, 185, 45, 15);
		panel.add(label_7);
		
		label_8 = new JLabel("");
		label_8.setBounds(58, 185, 86, 15);
		panel.add(label_8);
		btnEnter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int row = table.getSelectedRow();
				if(row == -1){
					JOptionPane.showMessageDialog(ChatList.this, "채팅방을 선택해주세요");
					return;
				}
				String title = table.getValueAt(row, 0).toString();
				ChatRoom frm = new ChatRoom(ChatList.this, title, 10);
				frm.setVisible(true);
				frm.setLocation(ChatList.this.getX() + 455, ChatList.this.getY());
				dao = new ChatListDAO();
				 model = new DefaultTableModel(dao.listRoom(), cols);
				// 테이블에 데이터가 채워짐
				table.setModel(model);
			}
		});
		
		
		
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String roomName="";
				roomName = JOptionPane.showInputDialog("방제목을 입력하세요.");
				int roomNo = table.getRowCount() + 1;
				System.out.println(roomNo);
				ChatListDTO dto = new ChatListDTO(roomNo, roomName);
				ChatListDAO dao = new ChatListDAO();
				if(roomName==null) return;
				int result = dao.createRoom(dto);
				if (result == 1) {
					// refreshTable();
					ChatRoom frm = new ChatRoom(ChatList.this);
					frm.setVisible(true);
					frm.setLocation(ChatList.this.getX() + 455, ChatList.this.getY());
					frm.setTitle(roomName);
					dao = new ChatListDAO();
					 model = new DefaultTableModel(dao.listRoom(), cols);
					// 테이블에 데이터가 채워짐
					table.setModel(model);
					
				}
			}
		});

		Thread t = new Thread(this);
		t.start();
	}


	@Override
	public void run() {
		// while (true) {
		dao = new ChatListDAO();
		 model = new DefaultTableModel(dao.listRoom(), cols);
		// 테이블에 데이터가 채워짐
		table.setModel(model);
		// refreshTable();
		// table.setVisible(true);
		try {
			Thread.sleep(500);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// }
	}
	public void showPlayer(){
		label_2.setText(id);
		
	}
}
