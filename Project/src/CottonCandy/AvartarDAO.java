package CottonCandy;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class AvartarDAO {
	public int avartarinput(AvartarDTO dto) {
		int result = 0;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.chatConn();
			String sql = "insert into avartar values (?,?,?,?,?,?)";
			
			pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, dto.getAvartarno());
			pstmt.setInt(2, dto.getSkin());
			pstmt.setInt(3, dto.getEyes());
			pstmt.setInt(4, dto.getHair());
			pstmt.setInt(5, dto.getCloth());
			pstmt.setInt(6, dto.getAcc());
			
			result = pstmt.executeUpdate();	
			
		} catch (Exception e) {
			
		}
		return result;
				
	}
	
//	public void ImageTrance(String sbStirng) {
//		String sbString = sbStirng;
//		ImageIcon ic  = new ImageIcon(sbString);
//	     JLabel lbImage1  = new JLabel(ic);
////	     ImageIcon imageIcon = new ImageIcon("./img/imageName.png"); // load the image to a imageIcon
//	     Image image = ic.getImage(); // transform it 
//	     Image newimg = image.getScaledInstance(300	, 300,  Image.SCALE_AREA_AVERAGING); // scale it the smooth way 
//	     image.
//	     BufferedImage mergedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
//		 Graphics2D graphics = (Graphics2D) mergedImage.getGraphics();
//	}
	
	public int avartarsave(AvartarDTO dto){
		
		int avartarno = dto.getAvartarno();
		int skin =0 ;
		int eyes =0 ;
		int hair =0 ;
		int cloth =0 ;
		int acc =0 ;
		
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DB.chatConn();
			String sql="select * from avartar where avartarno=?";
			
			pstmt=conn.prepareStatement(sql);
			pstmt.setInt(1, avartarno);
			rs = pstmt.executeQuery();
			if(rs.next()){
				skin=rs.getInt("skin");
				eyes=rs.getInt("eyes");
				hair=rs.getInt("hair");
				cloth=rs.getInt("cloth");
				acc=rs.getInt("acc");
				
			}
			
			try {
				BufferedImage image1 = ImageIO.read(new File("C:\\Users\\ssin7\\Desktop\\자바 교육\\프로젝트\\아바타 제작\\avatar\\skin\\skin"+skin+".png"));
				BufferedImage image2 = ImageIO.read(new File("C:\\Users\\ssin7\\Desktop\\자바 교육\\프로젝트\\아바타 제작\\avatar\\eye\\eyes"+eyes+".png"));
				BufferedImage image3 = ImageIO.read(new File("C:\\Users\\ssin7\\Desktop\\자바 교육\\프로젝트\\아바타 제작\\avatar\\hair\\hair"+hair+".png"));
				BufferedImage image4 = ImageIO.read(new File("C:\\Users\\ssin7\\Desktop\\자바 교육\\프로젝트\\아바타 제작\\avatar\\cloth\\cloth"+cloth+".png"));
				BufferedImage image5 = ImageIO.read(new File("C:\\Users\\ssin7\\Desktop\\자바 교육\\프로젝트\\아바타 제작\\avatar\\acc\\acc"+acc+".png"));

				int width = Math.max(image1.getWidth(), image5.getWidth());
				int height = image1.getHeight() + image5.getHeight();

				BufferedImage mergedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
				Graphics2D graphics = (Graphics2D) mergedImage.getGraphics();

				graphics.setBackground(Color.WHITE);
				graphics.drawImage(image1, 0, 0, null);;
				graphics.drawImage(image2, 6, 12, null);
				graphics.drawImage(image3, 0, 0, null);
				graphics.drawImage(image4, 4, 16, null);
				graphics.drawImage(image4, 12, 16, null);
				
				
				ImageIO.write(mergedImage, "gif", new File("C:\\Users\\ssin7\\Desktop\\자바 교육\\프로젝트\\아바타 제작\\avatar\\avatar.gif"));
				// ImageIO.write(mergedImage, "jpg", new File("c:\\mergedImage.jpg"));
				// ImageIO.write(mergedImage, "png", new File("c:\\mergedImage.png"));
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
			
		} catch (Exception e) {
			
		}
		return avartarno;
	}
	

}
