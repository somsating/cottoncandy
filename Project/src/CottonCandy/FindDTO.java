package CottonCandy;

public class FindDTO {
	private String name, email;
	public FindDTO() {
		// TODO Auto-generated constructor stub
	}
	public FindDTO(String name, String email) {
		super();
		this.name = name;
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
