package CottonCandy;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class Details extends JFrame {

	protected static final Object ADD = null;
	private JPanel contentPane;
	private DefaultTableModel model;
	private DTO2 dto;
	private int idx;

	private String sql;
	private Connection conn;
	private PreparedStatement pstmt;
	private ResultSet rs;
	private LIST frm;

	private Vector cols, col;
	private DAO2 dao;
	private JTextField tfBlockdate;
	private JLabel lblBlock;
	private JTextField tfBlock;
	private JTextField tfJoindate;
	private JLabel lblJoindate;
	private JButton btnUpdate;
	private JTable table;
	private JTextField tfIdx;
	private JButton btnAvatar;
	private JTextField tfSearch;
	private JComboBox comboBox;
	private JScrollPane scrollPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Details frame = new Details();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Details() {
		dao = new DAO2();
		cols = new Vector();
		cols.add("idx");
		cols.add("joindate");
		cols.add("block");
		cols.add("blockdate");

		// refreshTable();
		// System.out.println(cols);

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 535, 333);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
				scrollPane = new JScrollPane();
				scrollPane.setBounds(12, 60, 497, 196);
				contentPane.add(scrollPane);
				
						// member click input textarea
						table = new JTable(model);
						table.setVisible(true);
						table.addMouseListener(new MouseAdapter() {
							public void mouseClicked(MouseEvent e) {
								int idx = table.getSelectedRow();
								tfIdx.setEditable(false);
								tfIdx.setText(table.getValueAt(idx, 0) + "");
								tfJoindate.setText(table.getValueAt(idx, 1) + "");
								tfBlock.setText(table.getValueAt(idx, 2) + "");
								tfBlockdate.setText(table.getValueAt(idx, 3) + "");
								
							}
						});
						scrollPane.setViewportView(table);

		JLabel label = new JLabel("");
		label.setBounds(12, 96, 57, 15);
		contentPane.add(label);

		tfBlockdate = new JTextField();
		tfBlockdate.setColumns(10);
		tfBlockdate.setBounds(284, 29, 116, 21);
		contentPane.add(tfBlockdate);

		JLabel lblBlockdate = new JLabel("BlockDate");
		lblBlockdate.setBounds(215, 32, 79, 15);
		contentPane.add(lblBlockdate);

		lblBlock = new JLabel("BLOCK");
		lblBlock.setBounds(215, 7, 57, 15);
		contentPane.add(lblBlock);

		tfBlock = new JTextField();
		tfBlock.setColumns(10);
		tfBlock.setBounds(284, 4, 116, 21);
		contentPane.add(tfBlock);

		tfJoindate = new JTextField();
		tfJoindate.setColumns(10);
		tfJoindate.setBounds(81, 29, 116, 21);
		contentPane.add(tfJoindate);

		lblJoindate = new JLabel("JoinDate");
		lblJoindate.setBounds(12, 32, 57, 15);
		contentPane.add(lblJoindate);

		btnUpdate = new JButton("UPDATE");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input();
				int result = dao.updatechatMember(dto);
				if (result == 1) {
					JOptionPane.showMessageDialog(Details.this, "Complete");
					list();
					table.setModel(model);
					clear();
				}
			}
		});
		btnUpdate.setBounds(412, 28, 97, 23);
		contentPane.add(btnUpdate);

		JPanel panel = new JPanel();
		panel.setBounds(12, 60, 497, 196);
		contentPane.add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		JLabel lblNewLabel = new JLabel(" IDX");
		lblNewLabel.setBounds(12, 7, 57, 15);
		contentPane.add(lblNewLabel);

		tfIdx = new JTextField();
		tfIdx.setBounds(81, 4, 116, 21);
		contentPane.add(tfIdx);
		tfIdx.setColumns(10);

		btnAvatar = new JButton("AVATAR");
		btnAvatar.setBounds(412, 3, 97, 23);
		contentPane.add(btnAvatar);
		contentPane.setVisible(true);

		JButton button = new JButton("Search");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (idx) {
				case 0:
					String idx = tfSearch.getText();
					
					model = new DefaultTableModel(dao.searchidx(idx), cols);
					table.setModel(model);
					break;
				case 1:
					String joindate = tfSearch.getText();
					
					model = new DefaultTableModel(dao.searchjoindate(joindate), cols);
					table.setModel(model);
					break;
				case 2:
					String block = tfSearch.getText();

					model = new DefaultTableModel(dao.searchblock(block), cols);
					table.setModel(model);
					break;
				case 3:
					String blockdate = tfSearch.getText();

					model = new DefaultTableModel(dao.searchblockdate(blockdate), cols);
					table.setModel(model);
					break;

				}
			}
		});
		button.setBounds(313, 266, 97, 23);
		contentPane.add(button);

		tfSearch = new JTextField();
		tfSearch.setColumns(10);
		tfSearch.setBounds(185, 266, 116, 21);
		contentPane.add(tfSearch);

		JButton btnAllView = new JButton("All View");
		btnAllView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			clear();
			JOptionPane.showMessageDialog(Details.this, "Search for all Details.");
			String idx = tfSearch.getText();
			model = new DefaultTableModel(dao.searchidx(idx), cols);
			table.setModel(model);
			}
		});
		btnAllView.setBounds(412, 266, 97, 23);
		contentPane.add(btnAllView);

		comboBox = new JComboBox();

		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				idx = comboBox.getSelectedIndex();
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(
				new String[] { "Search by IDX", "Search by Join Date", "Search by Block", "Search by Block Date" }));
		comboBox.setBounds(22, 266, 151, 21);
		contentPane.add(comboBox);
		list();

	}

	public void input() {
		int idx = Integer.parseInt(tfIdx.getText());
		Date joindate = Date.valueOf(tfJoindate.getText());
		int block = Integer.parseInt(tfBlock.getText());
		Date blockdate = Date.valueOf(tfBlockdate.getText());

		dto = new DTO2(idx, joindate, block, blockdate);
	}

	public void clear() {
		tfIdx.setText("");
		tfJoindate.setText("");
		tfBlock.setText("");
		tfBlockdate.setText("");
		tfSearch.setText("");
	}

	public void search() {
		String chatMember = tfSearch.getText();
		model = new DefaultTableModel(dao.searchidx(chatMember), cols) {
			public boolean isCellEdiable(int row, int column) {
				return false;
			}
		};
		table.setModel(model);
	}

	public void list() {
		System.out.println(dao.listchatMember().size());
		model = new DefaultTableModel(dao.listchatMember(), cols) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table.setModel(model);

	}
}
