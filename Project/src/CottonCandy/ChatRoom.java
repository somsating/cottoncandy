package CottonCandy;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;

public class ChatRoom extends JFrame implements Runnable {

	private JPanel contentPane;
	private JTextField tfMsg;
	private JPanel panel_Partner;
	private JPanel panel_Me;
	private JButton btnEmoticon;
	private JScrollPane scrollPane;
	private JButton btnPicture;
	private JButton btnInput;
	private JButton btnGame;
	private JLabel lblMyNick;

	// 변수추가
	private ChatRoomDAO dao;
	private JTextPane text;
	private String img_path;
	private ChatList frm;
	private String title;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChatRoom frame = new ChatRoom();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public ChatRoom(ChatList frm){
		this();
		this.frm=frm;
	}
	public ChatRoom(ChatList frm,String title,int code){
		this();
		this.setTitle(title);
	}
	
	public ChatRoom() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 473, 439);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnGame = new JButton("▶");
		btnGame.setBounds(412, 0, 45, 400);
		contentPane.add(btnGame);

		btnInput = new JButton("전송");
		btnInput.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String nickName = lblMyNick.getText();
				String msg = tfMsg.getText();
				ChatRoomDTO dto = new ChatRoomDTO(nickName, msg);
				ChatRoomDAO dao = new ChatRoomDAO();
				int result = dao.inputMsg(dto);
				if (result == 1) {
					tfMsg.setText("");
					tfMsg.requestFocus();
				}
			}
		});
		btnInput.setBounds(345, 377, 67, 23);
		contentPane.add(btnInput);

		tfMsg = new JTextField();
		tfMsg.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String nickName = lblMyNick.getText();
				String msg = tfMsg.getText();
				ChatRoomDTO dto = new ChatRoomDTO(nickName, msg);
				ChatRoomDAO dao = new ChatRoomDAO();
				int result = dao.inputMsg(dto);
				if (result == 1) {
					tfMsg.setText("");
					tfMsg.requestFocus();
				}
			}
		});
		tfMsg.setBounds(128, 378, 215, 21);
		contentPane.add(tfMsg);
		tfMsg.setColumns(10);

		btnPicture = new JButton("사진올리기");
		btnPicture.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 파일선택 대화상자
//				JFileChooser fc = new JFileChooser();
//				int result = fc.showOpenDialog(ChatRoom.this);
//				if (result == JFileChooser.APPROVE_OPTION) {
//					// 선택한 이미지 파일의 절대경로
//					img_path = fc.getSelectedFile().getAbsolutePath();
//					System.out.println(img_path);
//					File file = fc.getSelectedFile(); // 파일참조변수
//					try {
//						// 이미지 아이콘 생성
//						ImageIcon icon = new ImageIcon(ImageIO.read(file));
//						// 원본 이미지
//						Image imageSrc = icon.getImage();
//						BufferedImage bi = ImageIO.read(file);
//						File copyFile = new File("test.jpg");
//						// 이미지,포맷형식,저장이름
//						ImageIO.write(bi, "jpg", copyFile);
//						// 사이즈가 조절된 이미지
//						Image imageNew = imageSrc.getScaledInstance(80, 100, Image.SCALE_AREA_AVERAGING);
//						icon = new ImageIcon(imageNew);
//						// JLabel에 이미지를 아이콘으로 표시
//						text.selectAll();
//						text.setSelectionStart(text.getSelectionEnd());
//						text.insertComponent(new JLabel(icon));
//					} catch (Exception e2) {
//						e2.printStackTrace();
//					}
//				}
			}
		});
		btnPicture.setBounds(0, 377, 93, 23);
		contentPane.add(btnPicture);

		btnEmoticon = new JButton("i");
		btnEmoticon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		btnEmoticon.setBounds(93, 377, 35, 23);
		contentPane.add(btnEmoticon);

		panel_Me = new JPanel();
		panel_Me.setBounds(0, 189, 93, 188);
		contentPane.add(panel_Me);
		panel_Me.setLayout(new BorderLayout(0, 0));

		lblMyNick = new JLabel("하이");
		lblMyNick.setHorizontalAlignment(SwingConstants.CENTER);
		panel_Me.add(lblMyNick, BorderLayout.NORTH);

		panel_Partner = new JPanel();
		panel_Partner.setBounds(0, 0, 93, 188);
		contentPane.add(panel_Partner);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(93, 0, 319, 377);
		text = new JTextPane();
		text.setEditable(false);
		scrollPane.setViewportView(text);
		contentPane.add(scrollPane);

		Thread t = new Thread(this);
		t.start();
	}

	@Override
	public void run() {
		while (true) {
			dao = new ChatRoomDAO();
			text.setText(dao.chatMsg());
			text.selectAll();
			text.setSelectionStart(text.getSelectionEnd());
			try {
				Thread.sleep(500);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
