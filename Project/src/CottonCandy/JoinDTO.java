package CottonCandy;

import java.sql.Date;

public class JoinDTO {
	private String id, pwd, nick, name, email, phone;
	private int sex, avatarcolor,avatarhair,avatareyes,avatarcloth,avataraccss;
	private Date birth, joinDate;
	
	public JoinDTO() {
		// TODO Auto-generated constructor stub
	}

	

	public JoinDTO(String id, String pwd, String nick, String name, String email, String phone, int sex,
			int avatarcolor, int avatarhair, int avatareyes, int avatarcloth, int avataraccss, Date birth,
			Date joinDate) {
		super();
		this.id = id;
		this.pwd = pwd;
		this.nick = nick;
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.sex = sex;
		this.avatarcolor = avatarcolor;
		this.avatarhair = avatarhair;
		this.avatareyes = avatareyes;
		this.avatarcloth = avatarcloth;
		this.avataraccss = avataraccss;
		this.birth = birth;
		this.joinDate = joinDate;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}



	public int getAvatarcolor() {
		return avatarcolor;
	}



	public void setAvatarcolor(int avatarcolor) {
		this.avatarcolor = avatarcolor;
	}



	public int getAvatarhair() {
		return avatarhair;
	}



	public void setAvatarhair(int avatarhair) {
		this.avatarhair = avatarhair;
	}



	public int getAvatareyes() {
		return avatareyes;
	}



	public void setAvatareyes(int avatareyes) {
		this.avatareyes = avatareyes;
	}



	public int getAvatarcloth() {
		return avatarcloth;
	}



	public void setAvatarcloth(int avatarcloth) {
		this.avatarcloth = avatarcloth;
	}



	public int getAvataraccss() {
		return avataraccss;
	}



	public void setAvataraccss(int avataraccss) {
		this.avataraccss = avataraccss;
	}



	public Date getJoinDate() {
		return joinDate;
	}



	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}
	
	
	
	
}
