package CottonCandy;

import java.sql.Date;

public class DTO {
	private int idx;
	private String id;
	private String pwd;
	private String nick;
	private int nickchange;
	private String name;
	private Date birth;
	private int sex;
	private String email;
	private String tel;

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public int getNickchange() {
		return nickchange;
	}

	public void setNickchange(int nickchange) {
		this.nickchange = nickchange;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirth() {
		return birth;
	}

	public void setBirth(Date birth) {
		this.birth = birth;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	@Override
	public String toString() {
		return "DTO [idx=" + idx + ", id=" + id + ", pwd=" + pwd + ", nick=" + nick + ", nickchange=" + nickchange
				+ ", name=" + name + ", birth=" + birth + ", sex=" + sex + ", email=" + email + ", tel=" + tel + "]";
	}

public DTO() {
	
}

	public DTO(int idx, String id, String pwd, String nick, int nickchange, String name, Date birth, int sex,
			String email, String tel) {
		super();
		this.idx = idx;
		this.id = id;
		this.pwd = pwd;
		this.nick = nick;
		this.nickchange = nickchange;
		this.name = name;
		this.birth = birth;
		this.sex = sex;
		this.email = email;
		this.tel = tel;
	}

}
