package CottonCandy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class DAO2 {

	// -------------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	// ---------------SEARCH-----------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	public Vector searchidx(String idx) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from chatMember where idx like ? order by idx");

			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + idx + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getString("joindate"));
				row.add(rs.getString("block"));
				row.add(rs.getString("blockdate"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}

	public Vector searchjoindate(String joindate) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from chatMember where joindate like ? order by joindate");

			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + joindate + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getString("joindate"));
				row.add(rs.getString("block"));
				row.add(rs.getString("blockdate"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}

	public Vector searchblock(String block) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from chatMember where block like ? order by block");

			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + block + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getString("joindate"));
				row.add(rs.getString("block"));
				row.add(rs.getString("blockdate"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}

	public Vector searchblockdate(String blockdate) {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select * from chatMember where blockdate like ? order by blockdate");

			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, "%" + blockdate + "%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getString("joindate"));
				row.add(rs.getString("block"));
				row.add(rs.getString("blockdate"));
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return items;
	}

	// -------------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	// ---------------SEARCH-----------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------------
	public int updatechatMember(DTO2 dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.chatConn();
			String sql = "update chatMember " + " set joindate=?, block=?, blockdate=?" + " where idx=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(4, dto.getIdx());
			pstmt.setDate(1, dto.getJoindate());
			pstmt.setInt(2, dto.getBlock());
			pstmt.setDate(3, dto.getBlockdate());
			result = pstmt.executeUpdate();
			System.out.println(result);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	public Vector listchatMember() {
		// TODO Auto-generated method stub
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			String sql = "select * from chatMember";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				row.add(rs.getInt("idx"));
				row.add(rs.getDate("joindate"));
				row.add(rs.getInt("block"));
				row.add(rs.getDate("blockdate"));
				items.add(row);
				System.out.println(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;
	}

	// public Vector searchchatMember(String joindate) {
	// Vector items = new Vector();
	// Connection conn = null;
	// PreparedStatement pstmt = null;
	// ResultSet rs = null;
	// try {
	// conn = DB.dbConn();
	// String sql = "select * from chatMember where idx like ? order by
	// joindate";
	// pstmt = conn.prepareStatement(sql);
	// pstmt.setString(11, "%" + joindate + "%");
	// rs = pstmt.executeQuery();
	// while (rs.next()) {
	// Vector row = new Vector();
	// row.add(rs.getInt("idx"));
	// row.add(rs.getDate("joindate"));
	// row.add(rs.getInt("block"));
	// row.add(rs.getInt("avatareyes"));
	// row.add(rs.getInt("avatarcolor"));
	// row.add(rs.getInt("avatarhair"));
	// row.add(rs.getInt("avatarcloth"));
	// row.add(rs.getInt("avataraccss"));
	// row.add(rs.getInt("avatarchange"));
	// items.add(row);
	// }
	// } catch (Exception e) {
	// e.printStackTrace();
	// } finally {
	// try {
	// if (rs != null)
	// rs.close();
	// } catch (SQLException e) {
	// e.printStackTrace();
	// }
	// try {
	// if (pstmt != null)
	// pstmt.close();
	// } catch (SQLException e) {
	// e.printStackTrace();
	// }
	// try {
	// if (conn != null)
	// conn.close();
	// } catch (SQLException e) {
	// e.printStackTrace();
	// }
	// }
	// return items;
	// }

	public int deletechatMember(String joindate) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.chatConn();
			String sql = "delete from chatMember where joindate=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(11, joindate);
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;

	}

}
