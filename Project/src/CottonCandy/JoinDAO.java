package CottonCandy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class JoinDAO {
	public int JoinId(JoinDTO dto) {
		int result = 0;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.chatConn();
			String sql = "insert into chatMember (id, pwd, nick, name, email, tel, "
					+ " birth, sex, joinDate, avatarcolor, avatarhair, avatareyes, avatarcloth, avataraccss)"
					+ " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, dto.getId());
			pstmt.setString(2, dto.getPwd());
			pstmt.setString(3, dto.getNick());
			pstmt.setString(4, dto.getName());
			pstmt.setString(5, dto.getEmail());
			pstmt.setString(6, dto.getPhone());
			pstmt.setDate(7, dto.getBirth());
			pstmt.setInt(8, dto.getSex());
			pstmt.setDate(9, dto.getJoinDate());
			pstmt.setInt(10, dto.getAvatarcolor());
			pstmt.setInt(11, dto.getAvatarhair());
			pstmt.setInt(12, dto.getAvatareyes());
			pstmt.setInt(13, dto.getAvatarcloth());
			pstmt.setInt(14, dto.getAvataraccss());
			result = pstmt.executeUpdate();
		} catch (Exception e) {

		}

		return result;	
	}
	public int CheckId(String id){
		int idx=0;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DB.chatConn();
			String sql="select idx from chatMember where id=?";
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, id);
			rs = pstmt.executeQuery();
			if(rs.next()){
				idx=rs.getInt("idx");
			}
		} catch (Exception e) {
			
		}
		return idx;
	}
	public int CheckNick(String nick){
		int idx=0;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DB.chatConn();
			String sql="select idx from chatMember where nick=?";
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, nick);
			rs = pstmt.executeQuery();
			if(rs.next()){
				idx=rs.getInt("idx");
			}
		} catch (Exception e) {
			
		}
		return idx;
	}
	
	public int CheckEmail(String email){
		int idx=0;
		
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			conn = DB.chatConn();
			String sql="select idx from chatMember where email=?";
			pstmt=conn.prepareStatement(sql);
			pstmt.setString(1, email);
			rs = pstmt.executeQuery();
			if(rs.next()){
				idx=rs.getInt("idx");
			}
		} catch (Exception e) {
			
		}
		return idx;
	}
}
