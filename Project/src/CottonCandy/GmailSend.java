package CottonCandy;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.JOptionPane;

public class GmailSend {

	private String subject;
	private String content;
	private String id;
	private int code;
	private Random random;
	
	public int sendMail(String sender, String id) {
		subject = "솜사팅 인증 메일입니다.";
		this.id=id;
		code=GenerateCode();
		contentMake();
		Properties p = System.getProperties();
		p.put("mail.smtp.starttls.enable", "true");
		p.put("mail.smtp.host", "smtp.gmail.com"); // smtp 서버 호스트
		p.put("mail.smtp.auth", "true");
		p.put("mail.smtp.port", "587"); // gmail 포트

		Authenticator auth = new MyAuthentication(); // 구글 계정 인증

		// session 생성 및 MimeMessage생성
		Session session = Session.getDefaultInstance(p, auth);
		MimeMessage msg = new MimeMessage(session);
		String fromName = "솜사팅"; // 닉네임
		String charSet = "UTF-8";

		try {
			// 메일 보낸 시간 설정
			msg.setSentDate(new Date());
			// 송신자 설정
			InternetAddress from = new InternetAddress();
			from = new InternetAddress(new String(fromName.getBytes(charSet), "8859_1") + "<somsatingmanager@gmail.com>");
			msg.setFrom(from);
			// 수신자 설정
			InternetAddress to = new InternetAddress(sender);
			msg.setRecipient(Message.RecipientType.TO, to);
			// 제목 설정
			msg.setSubject(subject, "UTF-8");
			msg.setText(content, "UTF-8"); // 내용 설정
			msg.setHeader("content-Type", "text/html");
			
			// 메일 송신
			Transport.send(msg);
			System.out.println("메일을 보냈습니다.");
		} catch (AddressException addr_e) { // 예외처리 주소를 입력하지 않을 경우
			JOptionPane.showMessageDialog(null, "메일을 입력해주세요", "메일주소입력", JOptionPane.ERROR_MESSAGE);
			addr_e.printStackTrace();
		} catch (MessagingException msg_e) { // 메시지에 이상이 있을 경우
			JOptionPane.showMessageDialog(null, "메일을 제대로 입력해주세요.", "오류발생", JOptionPane.ERROR_MESSAGE);
			msg_e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return code;
	}

	class MyAuthentication extends Authenticator {
		PasswordAuthentication pa;
		public MyAuthentication() { // 생성자를 통해 구글 ID/PW 인증
			String id = "somsatingmanager"; // 구글 ID
			String pw = "somsating8175"; // 구글 비밀번호
			// ID와 비밀번호를 입력한다.
			pa = new PasswordAuthentication(id, pw);
		}
		// 시스템에서 사용하는 인증정보
		public PasswordAuthentication getPasswordAuthentication() {
			return pa;
		}
	}
	public void contentMake() {
		content = "<html><div style=\"background-image: url('http://isg0416.ipdisk.co.kr/publist/HDD1/cottoncandy/CottonCandy.jpg');background-repeat: no-repeat;width: 1000px;height: 1000px;opacity: 0.5;text-align: center;position:relative;\"><img src=\"http://isg0416.ipdisk.co.kr/publist/HDD1/cottoncandy/bgtop.png\"><span style=\"font-size: 18px;\"><b>[솜사팅 회원 인증 메일]</b></span><br><br><br> "+ id +  " 계정의 이메일 인증을 위해<br>아래 인증번호를 인증화면에 입력하세요.<br><br>인증번호 : " + code + "</div></html>";			
	}
	public int GenerateCode(){
		random = new Random();
		code = random.nextInt(9000)+1000;
		return code;
	}
}
