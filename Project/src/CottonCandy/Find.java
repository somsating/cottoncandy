package CottonCandy;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JPasswordField;

public class Find extends JFrame {

	private JPanel contentPane;
	private JTextField tfName;
	private JTextField tfEmail;
	private String id="";
	private FindDTO dto;
	private FindDAO dao;
	private JTextField tfConfirm;
	private JButton btnConfirm;
	private JButton btnChange;
	private JLabel lblFind;
	private JButton btnSendMail;
	private Login login;
	private int x=0, y=0, width=250, height=150;
	private GmailSend send;
	private int code;
	private JLabel lblNewLabel_3;
	private JLabel lblpwdEqual;
	private String pwd1, pwd2;
	private JPasswordField pwf1;
	private JPasswordField pwf2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Find frame = new Find();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public Find(Login login){
		this();
		this.login=login;
	}
	
	public Find() {
		setTitle("ID/PW찾기");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(x, y, 250, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("이름 : ");
		lblNewLabel.setBounds(28, 13, 44, 15);
		contentPane.add(lblNewLabel);
		
		tfName = new JTextField();
		tfName.setBounds(94, 10, 116, 21);
		contentPane.add(tfName);
		tfName.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("이메일 : ");
		lblNewLabel_1.setBounds(28, 48, 57, 15);
		contentPane.add(lblNewLabel_1);
		
		tfEmail = new JTextField();
		tfEmail.setBounds(94, 45, 116, 21);
		contentPane.add(tfEmail);
		tfEmail.setColumns(10);
		
		JButton btnFind = new JButton("찾기");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dto=new FindDTO();
				dao = new FindDAO();
				if(tfName.getText().isEmpty()){
					JOptionPane.showMessageDialog(Find.this, "<html><span style=\"font-family: 맑은 고딕;\">이름을 입력하세요.</span></html>");
				} else if(tfEmail.getText().isEmpty()){
					JOptionPane.showMessageDialog(Find.this, "<html><span style=\"font-family: 맑은 고딕;\">이메일을 입력하세요.</span></html>");
				} else {
					dto.setName(tfName.getText());
					dto.setEmail(tfEmail.getText());
					FindId();
				}
			}
		});
		btnFind.setBounds(70, 79, 97, 23);
		contentPane.add(btnFind);
		
		lblFind = new JLabel("");
		lblFind.setBounds(28, 120, 182, 70);
		contentPane.add(lblFind);
		
		tfConfirm = new JTextField();
		tfConfirm.setBounds(12, 244, 116, 21);
		contentPane.add(tfConfirm);
		tfConfirm.setColumns(10);
		
		btnConfirm = new JButton("인증");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(code==Integer.valueOf(tfConfirm.getText())){
					JOptionPane.showMessageDialog(Find.this, "<html><span style=\"font-family: 맑은 고딕;\">인증되었습니다.</span></html>");
					height=450;
					ReSize();
					//setBounds(x, y-150, 250, 450);
				}
				else {
					JOptionPane.showMessageDialog(Find.this, "<html><span style=\"font-family: 맑은 고딕;\">정확한 인증번호를 입력하세요.</span></html>");
				}
			}
		});
		btnConfirm.setBounds(140, 244, 82, 23);
		contentPane.add(btnConfirm);
		
		btnChange = new JButton("변경");
		btnChange.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(lblpwdEqual.getText().equals("비밀번호가 일치합니다.")){
					int result=0;
					result=dao.UpdatePwd(id, pwd1);
					if (result == 1) {
						JOptionPane.showMessageDialog(Find.this, "<html><span style=\"font-family: 맑은 고딕;\">비밀번호가 변경되었습니다.</span></html>");
						login.FindId(id);
						dispose();
					}
				} else{
					JOptionPane.showMessageDialog(Find.this, "<html><span style=\"font-family: 맑은 고딕;\">비밀번호가 일치하지 않습니다.</span></html>");
				}
			}
		});
		btnChange.setBounds(140, 330, 82, 23);
		contentPane.add(btnChange);
		
		btnSendMail = new JButton("메일 인증");
		btnSendMail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				send = new GmailSend();
				code=send.sendMail(dto.getEmail(),id);
				//setBounds(x, y-85, 250, 320);
				height=320;
				ReSize();
			}
		});
		btnSendMail.setBounds(125, 200, 97, 23);
		contentPane.add(btnSendMail);
		
		JButton btnLogin = new JButton("로그인");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				login.FindId(id);
				dispose();
			}
		});
		btnLogin.setBounds(12, 200, 97, 23);
		contentPane.add(btnLogin);
		
		JLabel lblNewLabel_2 = new JLabel("비밀번호");
		lblNewLabel_2.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		lblNewLabel_2.setBounds(12, 291, 57, 15);
		contentPane.add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("비밀번호 확인");
		lblNewLabel_3.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		lblNewLabel_3.setBounds(12, 337, 97, 15);
		contentPane.add(lblNewLabel_3);
		
		lblpwdEqual = new JLabel("");
		lblpwdEqual.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		lblpwdEqual.setBounds(12, 387, 198, 15);
		contentPane.add(lblpwdEqual);
		
		pwf1 = new JPasswordField();
		pwf1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				pwd1=String.valueOf(pwf1.getPassword());
				pwdCheck();
			}
		});
		pwf1.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		pwf1.setBounds(12, 306, 116, 21);
		contentPane.add(pwf1);
		
		pwf2 = new JPasswordField();
		pwf2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				pwd2=String.valueOf(pwf2.getPassword());
				pwdCheck();
			}
		});
		pwf2.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		pwf2.setBounds(12, 356, 116, 21);
		contentPane.add(pwf2);
	}
	public void FindId() {
		id = dao.FindId(dto.getName(), dto.getEmail());
		FindXY();
		if(!id.isEmpty()){
			height=270;
			ReSize();
			//setBounds(x, y-60, 250, 270);
			lblFind.setText("<html><span style=\"font-family: 맑은 고딕;\">회원님의 ID는 "+id + "입니다.<br>"
					+ "비밀번호 변경을 원하시는 경우 메일 인증을 선택하세요.</span></html>");
		} else{
			height=220;
			ReSize();
			//setBounds(x, y-35, 250, 220);
			lblFind.setText("<html><span style=\"font-family: 맑은 고딕;\">회원 정보가 없습니다.</span></html>");
		}
	}
	public void FindXY(){
		x=login.getFindX();
		y=login.getFindY();
	}
	public void ReSize() {
		y=login.getFindY()-((int)(height-150)/2);
		setBounds(x, y, width, height);
	}
	public void pwdCheck(){
		if(pwd1.equals("")||pwd2.equals("")){
			lblpwdEqual.setText("");
		} else{
			if(pwd1.equals(pwd2)){
				lblpwdEqual.setForeground(Color.green);
				lblpwdEqual.setText("비밀번호가 일치합니다.");
			} else {
				lblpwdEqual.setForeground(Color.red);
				lblpwdEqual.setText("비밀번호가 일치하지 않습니다.");
			}
		}
	}
}
