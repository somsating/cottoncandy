package CottonCandy;

import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Emoticon extends JFrame {

	private JPanel contentPane;
	private Image[] emoB;
	private String[] emoA;
	private ImageIcon[] icon;
	private JLabel[] lbl;
	
	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Emoticon frame = new Emoticon();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the frame.
	 */
	public Emoticon() {
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 386, 508);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout());


		emoA = new String[60];
		for (int i = 0; i < 60; i++) {
			emoA[i] = "./emoticon/emoticons-" + String.valueOf(i + 1) + ".png";
			System.out.println(emoA[i]);
		}
		
		emoB = new Image[60];
		for(int i=0; i<60; i++){
			emoB[i] = Toolkit.getDefaultToolkit().getImage(getClass().getResource(emoA[i]));
		}
		
		icon= new ImageIcon[60];
		for(int i=0; i<60; i++){
			icon[i]=new ImageIcon(emoB[i]);
		}
	
		lbl=new JLabel[60];
		for (int i = 0; i < 60; i++) {
			lbl[i]=new JLabel(icon[i]);
		}
		
		for(int i=0; i<60; i++){
		contentPane.add(lbl[i]);
		}
	}
}
