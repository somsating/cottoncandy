package CottonCandy;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class Login extends JFrame {

	private JPanel contentPane;
	private JLabel lblBackImg;
	private Image backImg;
	private Color color;
	private JLabel lblId;
	private JLabel lblPw;
	private JTextField tfId;
	private JPasswordField pwf;
	private JButton btnLogin;
	private JButton btnFind;
	private String id, pwd, nick = "";
	private LoginDTO dto;
	private LoginDAO dao;
	private int LoginX, LoginY, LoginWidth, LoginHeight;
	private int FindX, FindY;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	public Login() {
		setTitle("솜사팅");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        int height = (int) ge.getMaximumWindowBounds().getHeight();
        int width = (int) ge.getMaximumWindowBounds().getWidth();
        
        LoginWidth=310;
        LoginHeight=435;
        LoginX=width/2 - LoginWidth/2;
        LoginY=height/2 - LoginHeight/2;
		setBounds(LoginX, LoginY, LoginWidth, LoginHeight);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		color = new Color(100, 100, 200, 100);
		dao = new LoginDAO();

		lblId = new JLabel("ID : ");
		lblId.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblId.setBounds(68, 270, 30, 15);
		contentPane.add(lblId);

		lblPw = new JLabel("PW : ");
		lblPw.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblPw.setBounds(68, 300, 36, 15);
		contentPane.add(lblPw);

		tfId = new JTextField();
		tfId.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tfId.setBounds(110, 268, 116, 21);
		contentPane.add(tfId);
		tfId.setColumns(10);

		pwf = new JPasswordField();
		pwf.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		pwf.setBounds(110, 297, 116, 21);
		contentPane.add(pwf);

		JButton btnJoin = new JButton("회원가입");
		btnJoin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Join joinForm = new Join();
				joinForm.setVisible(true);
				joinForm.setLocation(100, 100);
			}
		});
		btnJoin.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		btnJoin.setBounds(61, 325, 80, 23);
		contentPane.add(btnJoin);
		
		btnLogin = new JButton("로그인");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				id = tfId.getText();
				pwd = String.valueOf(pwf.getPassword());
				dto = new LoginDTO(id, pwd);
				nick = dao.Login(dto);
				if (nick != "") {
					if(nick.equals("흑설공주")){
						Admin adminForm = new Admin(id, nick);
						adminForm.setVisible(true);
						adminForm.setLocation(100, 100);
						dispose();
					}
					else{
					JOptionPane.showMessageDialog(Login.this, "<html><span style=\"font-family: 맑은 고딕;\">"+nick + "님 환영합니다.</span></html>");
					ChatList chatForm = new ChatList(id);
					chatForm.setVisible(true);
					chatForm.setLocation(100, 100);
					dispose();
					}
				} else {
					JOptionPane.showMessageDialog(Login.this,"<html><span style=\"font-family: 맑은 고딕;\">로그인에 실패했습니다.<br>정확한 ID/PW를 입력해주세요.</span></html>");
				}

			}
		});
		btnLogin.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		btnLogin.setBounds(146, 325, 80, 23);
		contentPane.add(btnLogin);

		btnFind = new JButton("ID / PW 찾기");
		btnFind.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Find findForm = new Find(Login.this);
				findForm.setVisible(true);
				FindX=Login.this.getX()+30;
				FindY=Login.this.getY()+145;
				findForm.setLocation(FindX, FindY);
			}
		});
		btnFind.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		btnFind.setBounds(61, 350, 165, 23);
		contentPane.add(btnFind);
		JLabel lblLoginForm = new JLabel("");
		lblLoginForm.setBackground(color);
		lblLoginForm.setBounds(40, 260, 210, 123);
		lblLoginForm.setOpaque(true);
		contentPane.add(lblLoginForm);

		lblBackImg = new JLabel("");
		lblBackImg.setBackground(Color.WHITE);
		lblBackImg.setBounds(0, 0, 300, 400);
		contentPane.add(lblBackImg);

		backImg = Toolkit.getDefaultToolkit().getImage(getClass().getResource("\\loginback.jpg"));
		ImageIcon icon = new ImageIcon(backImg);
		lblBackImg.setIcon(icon);
	}
	public void FindId(String id) {
		tfId.setText(id);
		pwf.requestFocus();
	}

	public int getFindX() {
		return FindX;
	}
	public int getFindY() {
		return FindY;
	}
}
