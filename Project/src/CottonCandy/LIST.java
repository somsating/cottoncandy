package CottonCandy;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

public class LIST extends JFrame {

	private JPanel contentPane;

	private Vector cols;
	private DAO dao;
	private DefaultTableModel model;
	private DTO dto;

	private JTable table;
	private JButton btnDetails;
	private JButton btnDeletemember;
	private JScrollPane scrollPane1;
	private JTextField tfName;
	private JLabel lblName;
	private JLabel lblBirth;
	private JLabel lblGender;
	private JLabel lblEmail;
	private JLabel lblTel;
	private JTextField tfTel;
	private JTextField tfEmail;
	private JTextField tfGender;
	private JTextField tfBirth;
	private JTextField tfIdx;
	private JLabel lblIdx;
	private JLabel lblId;
	private JTextField tfId;
	private JTextField tfPwd;
	private JLabel lblPwd;
	private JLabel lblNick;
	private JLabel lblNickchange;
	private JTextField tfNickchange;
	private JTextField tfNick;
	private JButton btnSearch;
	private JTextField tfSearch;
	private JButton btnUpdate;
	private JButton btnAdd;
	private JComboBox comboBox1;
	private int idx;
	private JLabel lblCheckId;
	private JLabel lblCheckNick;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LIST frame = new LIST();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LIST() {
		dao = new DAO();
		cols = new Vector();
		cols.add("idx");
		cols.add("id");
		cols.add("pwd");
		cols.add("nick");
		cols.add("nickchange");
		cols.add("name");
		cols.add("birth");
		cols.add("gender");
		cols.add("email");
		cols.add("tel");
		list();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 713, 484);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(5, 5, 874, 10);
		contentPane.add(panel);

		scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(5, 162, 680, 237);
		contentPane.add(scrollPane1);

		// member click input textarea
		table = new JTable(model);
		table.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				int idx = table.getSelectedRow();
				tfIdx.setEditable(false);
				tfIdx.setText(table.getValueAt(idx, 0) + "");
				tfId.setText(table.getValueAt(idx, 1) + "");
				tfPwd.setText(table.getValueAt(idx, 2) + "");
				tfNick.setText(table.getValueAt(idx, 3) + "");
				tfNickchange.setText(table.getValueAt(idx, 4) + "");
				tfName.setText(table.getValueAt(idx, 5) + "");
				tfBirth.setText(table.getValueAt(idx, 6) + "");
				tfGender.setText(table.getValueAt(idx, 7) + "");
				tfEmail.setText(table.getValueAt(idx, 8) + "");
				tfTel.setText(table.getValueAt(idx, 9) + "");
			}
		});
		scrollPane1.setViewportView(table);

		btnDetails = new JButton("DETAILS");
		btnDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Details inputdetails = new Details();
				inputdetails.setVisible(true);
				inputdetails.setLocation(300, 300);
			}
		});
		btnDetails.setBounds(568, 75, 107, 23);
		contentPane.add(btnDetails);

		btnDeletemember = new JButton("DELETE");
		btnDeletemember.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// int idx = tfIdx.getint();
				String idx = tfIdx.getText();
				int result = 0;
				int response = JOptionPane.showConfirmDialog(LIST.this, "Delete?");
				if (response == JOptionPane.YES_OPTION) {
					result = dao.deletechatMember(idx);
				}
				if (result == 1) {
					JOptionPane.showMessageDialog(LIST.this, "Complete.");
					list();
					refreshTable();
					clear();
				}
			}
		});
		btnDeletemember.setBounds(568, 102, 107, 23);
		contentPane.add(btnDeletemember);

		tfName = new JTextField();
		tfName.setColumns(10);
		tfName.setBounds(233, 75, 116, 21);
		contentPane.add(tfName);

		lblName = new JLabel("Name");
		lblName.setBounds(164, 78, 57, 15);
		contentPane.add(lblName);

		lblBirth = new JLabel("Birth");
		lblBirth.setBounds(164, 106, 57, 15);
		contentPane.add(lblBirth);

		lblGender = new JLabel("Gender");
		lblGender.setBounds(358, 78, 57, 15);
		contentPane.add(lblGender);

		lblEmail = new JLabel("Email");
		lblEmail.setBounds(164, 134, 57, 15);
		contentPane.add(lblEmail);

		lblTel = new JLabel("Tel");
		lblTel.setBounds(358, 106, 57, 15);
		contentPane.add(lblTel);

		tfTel = new JTextField();
		tfTel.setColumns(10);
		tfTel.setBounds(427, 103, 116, 21);
		contentPane.add(tfTel);

		tfEmail = new JTextField();
		tfEmail.setColumns(10);
		tfEmail.setBounds(233, 131, 116, 21);
		contentPane.add(tfEmail);

		tfGender = new JTextField();
		tfGender.setColumns(10);
		tfGender.setBounds(427, 75, 116, 21);
		contentPane.add(tfGender);

		tfBirth = new JTextField();
		tfBirth.setColumns(10);
		tfBirth.setBounds(233, 103, 116, 21);
		contentPane.add(tfBirth);

		tfIdx = new JTextField();
		tfIdx.setColumns(10);
		tfIdx.setBounds(233, 20, 116, 21);
		contentPane.add(tfIdx);

		lblIdx = new JLabel("IDX");
		lblIdx.setBounds(164, 23, 57, 15);
		contentPane.add(lblIdx);

		lblId = new JLabel("ID");
		lblId.setBounds(358, 25, 57, 15);
		contentPane.add(lblId);

		tfId = new JTextField();
		tfId.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (!tfId.getText().equals("")) {
					int idx = dao.CheckId(tfId.getText());
					if (idx == 0) {
						lblCheckId.setForeground(Color.black);
						lblCheckId.setText("Can use this ID.");
					} else {
						lblCheckId.setForeground(Color.red);
						lblCheckId.setText("ID is already in use.");
					}
				}
			}
		});
		tfId.setColumns(10);
		tfId.setBounds(426, 21, 116, 21);
		contentPane.add(tfId);

		tfPwd = new JTextField();
		tfPwd.setColumns(10);
		tfPwd.setBounds(233, 48, 116, 21);
		contentPane.add(tfPwd);

		lblPwd = new JLabel("PWD");
		lblPwd.setBounds(164, 52, 57, 15);
		contentPane.add(lblPwd);

		lblNick = new JLabel("Nick");
		lblNick.setBounds(358, 51, 57, 15);
		contentPane.add(lblNick);

		lblNickchange = new JLabel("Nickchange");
		lblNickchange.setBounds(358, 134, 67, 15);
		contentPane.add(lblNickchange);

		tfNickchange = new JTextField();
		tfNickchange.setColumns(10);
		tfNickchange.setBounds(427, 131, 116, 21);
		contentPane.add(tfNickchange);

		tfNick = new JTextField();
		tfNick.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (!tfNick.getText().equals("")) {
					int idx = dao.CheckNick(tfNick.getText());
					if (idx == 0) {
						lblCheckNick.setForeground(Color.black);
						lblCheckNick.setText("Can use this Name.");
					} else {
						lblCheckNick.setForeground(Color.red);
						lblCheckNick.setText("Name is already in use.");
					}
				}
			}
		});
		tfNick.setColumns(10);
		tfNick.setBounds(426, 47, 116, 21);
		contentPane.add(tfNick);

		btnSearch = new JButton("Search");
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch (idx) {
				case 0:
					String idx = tfSearch.getText();

					model = new DefaultTableModel(dao.searchidx(idx), cols);
					table.setModel(model);
					break;
				case 1:
					String id = tfSearch.getText();

					model = new DefaultTableModel(dao.searchid(id), cols);
					table.setModel(model);
					break;
				case 2:
					String nick = tfSearch.getText();

					model = new DefaultTableModel(dao.searchnick(nick), cols);
					table.setModel(model);
					break;
				case 3:
					String tel = tfSearch.getText();

					model = new DefaultTableModel(dao.searchtel(tel), cols);
					table.setModel(model);
					break;
				case 4:
					String birth = tfSearch.getText();

					model = new DefaultTableModel(dao.searchbirth(birth), cols);
					table.setModel(model);
					break;
				case 5:
					String email = tfSearch.getText();

					model = new DefaultTableModel(dao.searchemail(email), cols);
					table.setModel(model);
					break;
				}
			}
		});
		btnSearch.setBounds(479, 409, 97, 23);
		contentPane.add(btnSearch);

		tfSearch = new JTextField();
		tfSearch.addKeyListener(new KeyAdapter() {

			// public void keyReleased(KeyEvent e) {
			// search();
			// }
		});

		tfSearch.setBounds(351, 409, 116, 21);
		contentPane.add(tfSearch);
		tfSearch.setColumns(10);

		btnUpdate = new JButton("EDIT");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				input();
				int result = dao.updatechatMember(dto);
				if (result == 1) {
					// System.out.println(dto);
					JOptionPane.showMessageDialog(LIST.this, "Colplete");
					list();
					table.setModel(model);
					clear();
				}
			}
		});
		btnUpdate.setBounds(568, 129, 107, 23);
		contentPane.add(btnUpdate);

		comboBox1 = new JComboBox();
		comboBox1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				idx = comboBox1.getSelectedIndex();

			}
		});
		comboBox1.setModel(new DefaultComboBoxModel(new String[] { "Search by IDX", "Search by ID",
				"Search by Nickname", "Search by Tel number", "Search by Birth date", "Search by Email Address" }));
		comboBox1.setBounds(194, 409, 145, 21);
		contentPane.add(comboBox1);

		JButton btnFullview = new JButton("All View");
		btnFullview.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				clear();
				JOptionPane.showMessageDialog(LIST.this, "Search for all members.");
				String idx = tfSearch.getText();
				model = new DefaultTableModel(dao.searchidx(idx), cols);
				table.setModel(model);
			}
		});
		btnFullview.setBounds(578, 409, 97, 23);
		contentPane.add(btnFullview);

		lblCheckId = new JLabel("");
		lblCheckId.setBounds(556, 25, 129, 15);
		contentPane.add(lblCheckId);

		lblCheckNick = new JLabel("");
		lblCheckNick.setBounds(554, 50, 143, 15);
		contentPane.add(lblCheckNick);

		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon("C:\\Users\\user\\Desktop\\프로젝트\\chat_project171207\\maincharacter.jpg"));
		btnNewButton.setBounds(15, 19, 107, 133);
		contentPane.add(btnNewButton);
		refreshTable();
	}

	public void clear() {
		tfIdx.setText("");
		tfId.setText("");
		tfNick.setText("");
		tfNickchange.setText("");
		tfName.setText("");
		tfEmail.setText("");
		tfTel.setText("");
		tfBirth.setText("");
		tfGender.setText("");
		tfPwd.setText("");
		tfSearch.setText("");
		tfIdx.setEditable(true);
	}

	public void input() {
		int idx = Integer.parseInt(tfIdx.getText());
		String id = tfId.getText();
		String pwd = tfPwd.getText();
		String nick = tfNick.getText();
		int nickchange = Integer.parseInt(tfNickchange.getText());
		String name = tfName.getText();
		Date birth = Date.valueOf(tfBirth.getText());// #3
		int sex = Integer.parseInt(tfGender.getText());
		String email = tfEmail.getText();
		String tel = tfTel.getText();
		dto = new DTO(idx, id, pwd, nick, nickchange, name, birth, sex, email, tel);
		// System.out.println(dto);
	}

	// public void search() {
	// String idx = tfSearch.getText();
	// model = new DefaultTableModel(dao.searchchatMember(idx), col) {
	// @Override
	// public boolean isCellEditable(int row, int column) {
	// // TODO Auto-generated method stub
	// return false;
	// }
	// };
	// table.setModel(model);
	// }

	public void refreshTable() {
		DefaultTableModel model = new DefaultTableModel(dao.listchatMember(), cols);
		// System.out.println(dao.listchatMember());
		table.setModel(model);
	}

	public void search() {
		String chatMember = tfSearch.getText();
		model = new DefaultTableModel(dao.searchid(chatMember), cols) {
			public boolean isCellEdiable(int row, int column) {
				return false;
			}
		};
		table.setModel(model);
	}

	public void list() {
		model = new DefaultTableModel(dao.listchatMember(), cols) {
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
	}

	public void duplicateid() {
		if (idx == 0) {
			lblCheckId.setText("");
		} else {
			lblCheckNick.setText("ID duplication");
		}
	}
}
