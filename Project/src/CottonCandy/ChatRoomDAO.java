package CottonCandy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class ChatRoomDAO {

	public int inputMsg(ChatRoomDTO dto){
		int result = 0;
		Connection conn= null;
		PreparedStatement pstmt = null;
		try { 
			conn = DB.chatConn(); // 오라클 서버에 접속
			StringBuilder sb = new StringBuilder();
			sb.append("insert into chatRoom ");
			sb.append("values (?, ?, now())");
			pstmt = conn.prepareStatement(sb.toString());
			pstmt.setString(1, dto.getNickName());
			pstmt.setString(2, dto.getMsg());
			// 성공하면 1, 실패하면 0리턴
			// affected rows(영향을 받은 행의 수)
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	public String chatMsg(){
		StringBuilder sbb = new StringBuilder();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select nickName, msg ");
			sb.append(" from chatRoom");
			sb.append(" order by msgTime");
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				sbb.append(rs.getString("nickName")+" : "
						+rs.getString("msg")+"\n");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return sbb.toString();	
	}
	
}
