package CottonCandy;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Confirm extends JFrame {

	private JPanel contentPane;
	private JTextField tfConfirm;
	private JButton btnConfirm;
	private GmailSend send;
	private int code=0;
	private String email="", id="";
	private Join join;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					Confirm frame = new Confirm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Create the frame.
	 */
	public Confirm(String email, String id, Join join) {
		this();
		this.email=email;
		this.id=id;
		this.join=join;
		mail();
	}
	public Confirm() {		
		setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		setTitle("이메일 인증");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 260, 130);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("메일로 발송된 인증번호를 입력하세요.");
		lblNewLabel.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		lblNewLabel.setBounds(20
				, 20, 216, 15);
		contentPane.add(lblNewLabel);
		
		tfConfirm = new JTextField();
		tfConfirm.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tfConfirm.setBounds(20, 55, 116, 21);
		contentPane.add(tfConfirm);
		tfConfirm.setColumns(10);
		
		btnConfirm = new JButton("인증");
		btnConfirm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(code==Integer.valueOf(tfConfirm.getText())){
					JOptionPane.showMessageDialog(Confirm.this, "<html><span style=\"font-family: 맑은 고딕;\">인증되었습니다.</span></html>");
					join.Confirm();
					dispose();
				}
				else {
					JOptionPane.showMessageDialog(Confirm.this, "<html><span style=\"font-family: 맑은 고딕;\">정확한 인증번호를 입력하세요.</span></html>");
				}
			}
		});
		btnConfirm.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		btnConfirm.setBounds(148, 54, 84, 23);
		contentPane.add(btnConfirm);
	}
	
	public void mail(){
		send = new GmailSend();
		if(!email.equals("")){
			code=send.sendMail(email,id);
		}
	}
}
