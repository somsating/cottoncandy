package CottonCandy;

import java.sql.Date;

public class ChatRoomDTO {
	private String nickName;
	private String msg;
	private Date msgTime;
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Date getMsgTime() {
		return msgTime;
	}
	public void setMsgTime(Date msgTime) {
		this.msgTime = msgTime;
	}
	@Override
	public String toString() {
		return "ChatRoomDTO [nickName=" + nickName + ", msg=" + msg + ", msgTime=" + msgTime + "]";
	}
	public ChatRoomDTO() {
		super();
	}
	public ChatRoomDTO(String nickName, String msg) {
		super();
		this.nickName = nickName;
		this.msg = msg;
	}
	
}
