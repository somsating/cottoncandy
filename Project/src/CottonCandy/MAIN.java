package CottonCandy;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class MAIN extends JFrame {

	private JPanel contentPane;
	private JTextField tfinputid;
	private JPasswordField tfinputpwd;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MAIN frame = new MAIN();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MAIN() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 320, 161);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon("C:\\Users\\user\\Desktop\\프로젝트\\Member starting screen.jpg"));
		btnNewButton.setBounds(12, 10, 283, 41);
		contentPane.add(btnNewButton);

		JLabel lblid = new JLabel("ManagerID");
		lblid.setBounds(12, 64, 75, 15);
		contentPane.add(lblid);

		tfinputid = new JTextField();
		tfinputid.setBounds(91, 61, 116, 21);
		contentPane.add(tfinputid);
		tfinputid.setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(12, 92, 75, 15);
		contentPane.add(lblPassword);

		JButton btnNewButton_1 = new JButton("Login");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String inputid = tfinputid.getText();
				String inputpwd = tfinputpwd.getText();
				String loginid = "blacksnow";
				String loginpwd = "princess";

				if (loginid.equals(inputid) && loginpwd.equals(inputpwd)) {
					LIST frm = new LIST();
					JOptionPane.showMessageDialog(MAIN.this, "Run Administrator mode.");
					dispose();
					frm.setVisible(true);
					frm.setLocation(100, 100);
					
				} else {
					JOptionPane.showMessageDialog(MAIN.this, "Login information is incorrect.");
				}
			}
		});
		btnNewButton_1.setBounds(220, 88, 75, 23);
		contentPane.add(btnNewButton_1);

		tfinputpwd = new JPasswordField();
		tfinputpwd.setBounds(91, 89, 116, 22);
		contentPane.add(tfinputpwd);
	}
}
