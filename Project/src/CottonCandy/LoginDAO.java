package CottonCandy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class LoginDAO {
	public String Login(LoginDTO dto) {
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String id=dto.getUserid(), pwd=dto.getUserpw(), nickName="";
		
		try {
			conn=DB.chatConn();
			String sql = "select nick from chatMember where id=? and pwd=?";
			pstmt=conn.prepareStatement(sql);
			System.out.println(id + " " + pwd);
			pstmt.setString(1, id);
			pstmt.setString(2, pwd);
			rs=pstmt.executeQuery();
			if(rs.next()){
				nickName=rs.getString("nick");
			}
		} catch (Exception e) {
		}
		return nickName;
	}
}
