package CottonCandy;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

public class ChatListDAO {

	public int createRoom(ChatListDTO dto) {
		int result = 0;
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("insert into chatList ");
			sb.append(" values (?,?)");
			pstmt = conn.prepareStatement(sb.toString());
			System.out.println(dto);
			pstmt.setInt(1, dto.getRoomNo());
			pstmt.setString(2, dto.getRoomName());
			result = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	public Vector listRoom() {
		Vector items = new Vector();
		Connection conn = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			conn = DB.chatConn();
			StringBuilder sb = new StringBuilder();
			sb.append("select roomName,roomNo ");
			sb.append(" from chatList order by roomNO");
			pstmt = conn.prepareStatement(sb.toString());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				Vector row = new Vector();
				int roomNo = rs.getInt("roomNo");
				String roomName = rs.getString("roomName");
				row.add(roomName);
				row.add(roomNo);
				items.add(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (rs != null)
					rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (pstmt != null)
					pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return items;
	}

}
