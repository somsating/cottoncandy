package CottonCandy;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Date;
import java.util.Calendar;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;

import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class Join extends JFrame {

	private JPanel contentPane;
	private JTextField tfId;
	private JPasswordField pwf1;
	private JPasswordField pwf2;
	private JTextField tfNick;
	private JTextField tfName;
	private JTextField tfBirth;
	private JTextField tfEmail;
	private JTextField tfPhone;
	private JoinDAO dao;
	private JoinDTO dto;
	private JLabel lblCheckId;
	private JLabel lblCheckPwd;
	private JLabel lblCheckNick;
	private JLabel lblCheckEmail;
	private boolean[] check = { false, false, false, false };
	private String pwd1 = "", pwd2 = "";
	private int confirmX = 0, confirmY = 0;
	private String id, nick, name, email, phone;
	private int sex = 0;
	private Date birth, joinday;
	private Calendar cal = Calendar.getInstance();
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private int[] avatar = { 0, 0, 0, 0, 0 };
	private JLabel lblSkin;
	private JLabel lblHair;
	private JLabel lblEyes;
	private JLabel lblCloth;
	private JLabel lblAcc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Join frame = new Join();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Join() {
		dao = new JoinDAO();

		setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		setTitle("회원가입");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 390, 370);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("아이디 : ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblNewLabel.setBounds(50, 30, 57, 15);
		contentPane.add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("비밀번호 : ");
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_1.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblNewLabel_1.setBounds(38, 75, 69, 15);
		contentPane.add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("비밀번호 확인 : ");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_2.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblNewLabel_2.setBounds(12, 105, 95, 15);
		contentPane.add(lblNewLabel_2);

		JLabel lblNewLabel_3 = new JLabel("닉네임 : ");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_3.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblNewLabel_3.setBounds(50, 150, 57, 15);
		contentPane.add(lblNewLabel_3);

		tfId = new JTextField();
		tfId.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (!tfId.getText().equals("")) {
					id=tfId.getText();
					int idx = dao.CheckId(id);
					if (idx == 0) {
						lblCheckId.setForeground(Color.green);
						lblCheckId.setText("사용 가능한 ID입니다.");
						check[0]=true;
					} else {
						lblCheckId.setForeground(Color.red);
						lblCheckId.setText("이미 사용중인 ID입니다.");
						check[0]=false;
					}
				}
			}
		});
		tfId.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tfId.setBounds(119, 28, 116, 21);
		contentPane.add(tfId);
		tfId.setColumns(10);

		pwf1 = new JPasswordField();
		pwf1.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				pwd1 = String.valueOf(pwf1.getPassword());
				pwdCheck();
			}
		});
		pwf1.setBounds(119, 74, 116, 21);
		contentPane.add(pwf1);

		pwf2 = new JPasswordField();
		pwf2.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				pwd2 = String.valueOf(pwf2.getPassword());
				pwdCheck();
			}
		});
		pwf2.setBounds(119, 104, 116, 21);
		contentPane.add(pwf2);

		tfNick = new JTextField();
		tfNick.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (!tfNick.getText().equals("")) {
					nick=tfNick.getText();
					int idx = dao.CheckNick(nick);
					if (idx == 0) {
						lblCheckNick.setForeground(Color.green);
						lblCheckNick.setText("사용 가능한 닉네임입니다.");
						check[2]=true;
					} else {
						lblCheckNick.setForeground(Color.red);
						lblCheckNick.setText("이미 사용중인 닉네임입니다.");
						check[2]=false;
					}
				}
			}
		});
		tfNick.setBounds(119, 149, 116, 21);
		contentPane.add(tfNick);
		tfNick.setColumns(10);

		JLabel lblNewLabel_4 = new JLabel("이름 : ");
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_4.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblNewLabel_4.setBounds(50, 195, 57, 15);
		contentPane.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("생년월일 : ");
		lblNewLabel_5.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_5.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblNewLabel_5.setBounds(12, 225, 95, 15);
		contentPane.add(lblNewLabel_5);

		JLabel lblNewLabel_6 = new JLabel("이메일 : ");
		lblNewLabel_6.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_6.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblNewLabel_6.setBounds(12, 255, 95, 15);
		contentPane.add(lblNewLabel_6);

		JLabel lblNewLabel_7 = new JLabel("연락처 : ");
		lblNewLabel_7.setHorizontalAlignment(SwingConstants.RIGHT);
		lblNewLabel_7.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblNewLabel_7.setBounds(50, 300, 57, 15);
		contentPane.add(lblNewLabel_7);

		tfName = new JTextField();
		tfName.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				name=tfName.getText();
			}
		});
		tfName.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tfName.setBounds(119, 194, 116, 21);
		contentPane.add(tfName);
		tfName.setColumns(10);

		tfBirth = new JTextField();
		tfBirth.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if(!tfBirth.getText().equals("")){
				birth=Date.valueOf(tfBirth.getText());
				}
			}
		});
		tfBirth.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tfBirth.setBounds(119, 224, 116, 21);
		contentPane.add(tfBirth);
		tfBirth.setColumns(10);

		tfEmail = new JTextField();
		tfEmail.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				if (!tfEmail.getText().equals("")) {
					email=tfEmail.getText();
					int idx = dao.CheckEmail(email);
					if(!lblCheckEmail.getText().equals("이메일 인증이 완료되었습니다.")){
					if (idx == 0) {
						lblCheckEmail.setForeground(Color.green);
						lblCheckEmail.setText("사용 가능한 이메일입니다.");
						check[3]=false;
					} else {
						lblCheckEmail.setForeground(Color.red);
						lblCheckEmail.setText("이미 사용중인 이메일입니다.");
						check[3]=false;
					}
					}
				}
			}
		});
		tfEmail.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tfEmail.setBounds(119, 254, 116, 21);
		contentPane.add(tfEmail);
		tfEmail.setColumns(10);

		tfPhone = new JTextField();
		tfPhone.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				phone=tfPhone.getText();
			}
		});
		tfPhone.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		tfPhone.setBounds(119, 299, 116, 21);
		contentPane.add(tfPhone);
		tfPhone.setColumns(10);

		JButton btnAvatar = new JButton("아바타 설정");
		btnAvatar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					AvartarEdit aEdit = new AvartarEdit(Join.this);
					aEdit.setVisible(true);
					aEdit.setLocation(100, 100);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});
		
		lblAcc = new JLabel("");
		lblAcc.setBounds(250, 30, 100, 100);
		contentPane.add(lblAcc);
				lblCloth = new JLabel("");
				lblCloth.setBounds(250, 30, 100, 100);
				contentPane.add(lblCloth);
				lblEyes = new JLabel("");
				lblEyes.setBounds(250, 30, 100, 100);
				contentPane.add(lblEyes);
				lblHair = new JLabel("");
				lblHair.setBounds(250, 30, 100, 100);
				contentPane.add(lblHair);
		
				lblSkin = new JLabel("아바타 없음");
				lblSkin.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
				lblSkin.setHorizontalAlignment(SwingConstants.CENTER);
				lblSkin.setBounds(250, 30, 100, 100);
				contentPane.add(lblSkin);
		btnAvatar.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		btnAvatar.setBounds(253, 140, 97, 23);
		contentPane.add(btnAvatar);

		JButton btnJoin = new JButton("회원가입");
		btnJoin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int chk=0, result=0;
				for(int i=0;i<4;i++){
					if(check[i]==true){
						chk++;
					}
				}
				if(chk==4){
					joinday=new Date(cal.getTimeInMillis());
					dto = new JoinDTO(id,pwd1,nick,name,email,phone,1,avatar[0],avatar[1],avatar[2],avatar[3],avatar[4],birth,joinday);
					result=dao.JoinId(dto);
					if(result==1){
						JOptionPane.showMessageDialog(Join.this, "<html><span style=\"font-family: 맑은 고딕;\">회원가입이 완료되었습니다.</span></html>");
						dispose();
					} else {
						JOptionPane.showMessageDialog(Join.this, "<html><span style=\"font-family: 맑은 고딕;\">가입 오류<br>관리자에게 문의하세요.</span></html>");
					}
				}
				else{
					JOptionPane.showMessageDialog(Join.this, "<html><span style=\"font-family: 맑은 고딕;\">필수 항목(ID/PW/닉네임/Email)을 정확히 입력하세요.</span></html>");
				}
			}
		});
		btnJoin.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		btnJoin.setBounds(253, 296, 97, 23);
		contentPane.add(btnJoin);

		lblCheckId = new JLabel("");
		lblCheckId.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblCheckId.setBounds(80, 54, 150, 15);
		contentPane.add(lblCheckId);

		lblCheckPwd = new JLabel("");
		lblCheckPwd.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblCheckPwd.setBounds(80, 129, 150, 15);
		contentPane.add(lblCheckPwd);

		lblCheckNick = new JLabel("");
		lblCheckNick.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblCheckNick.setBounds(80, 174, 150, 15);
		contentPane.add(lblCheckNick);

		lblCheckEmail = new JLabel("");
		lblCheckEmail.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		lblCheckEmail.setBounds(80, 279, 190, 15);
		contentPane.add(lblCheckEmail);
		
		JButton btnEmail = new JButton("이메일 인증");
		btnEmail.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Confirm confirmForm = new Confirm(tfEmail.getText(),tfId.getText(),Join.this);
				confirmForm.setVisible(true);
				confirmX=Join.this.getX()+100;
				confirmY=Join.this.getY()+150;
				confirmForm.setLocation(confirmX, confirmY);
			}
		});
		btnEmail.setFont(new Font("맑은 고딕", Font.PLAIN, 11));
		btnEmail.setBounds(253, 251, 97, 23);
		contentPane.add(btnEmail);
		
		JRadioButton rdbtnMan = new JRadioButton("남");
		rdbtnMan.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(rdbtnMan.isSelected()){
					sex=1;
				}
			}
		});
		buttonGroup.add(rdbtnMan);
		rdbtnMan.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		rdbtnMan.setBounds(253, 221, 47, 23);
		contentPane.add(rdbtnMan);
		
		JRadioButton rdbtnWoman = new JRadioButton("여");
		rdbtnWoman.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(rdbtnWoman.isSelected()){
					sex=2;
				}
			}
		});
		buttonGroup.add(rdbtnWoman);
		rdbtnWoman.setFont(new Font("맑은 고딕", Font.PLAIN, 12));
		rdbtnWoman.setBounds(305, 221, 45, 23);
		contentPane.add(rdbtnWoman);
	}

	public void pwdCheck() {
		if (pwd1.equals("") || pwd2.equals("")) {
			lblCheckPwd.setText("");
		} else {
			if (pwd1.equals(pwd2)) {
				lblCheckPwd.setForeground(Color.green);
				lblCheckPwd.setText("비밀번호가 일치합니다.");
				check[1] = true;
			} else {
				lblCheckPwd.setForeground(Color.red);
				lblCheckPwd.setText("비밀번호가 일치하지 않습니다.");
				check[1] = false;
			}
		}
	}

	public void Confirm() {
		lblCheckEmail.setText("이메일 인증이 완료되었습니다.");
		check[3] = true;
	}

	public void Avatar(int[] avatar) {
		this.avatar = avatar;

		String skinsb = "skin\\skin" + avatar[0] + ".png";
		ImageIcon skin = new ImageIcon(skinsb);
		Image image = skin.getImage(); // transform it
		Image newimg = image.getScaledInstance(100, 100, Image.SCALE_AREA_AVERAGING);
		skin = new ImageIcon(newimg);
		lblSkin.setText("");
		lblSkin.setIcon(skin);

		String hairsb = "hair\\hair" + avatar[1] + ".png";
		ImageIcon hair = new ImageIcon(hairsb);
		image = hair.getImage(); // transform it
		newimg = image.getScaledInstance(85, 35, Image.SCALE_AREA_AVERAGING);
		hair = new ImageIcon(newimg);
		lblHair.setText("");
		lblHair.setIcon(hair);
		lblHair.setLocation(255, -5);

		String eyessb = "eyes\\eyes" + avatar[2] + ".png";
		ImageIcon eyes = new ImageIcon(eyessb);
		image = eyes.getImage(); // transform it
		newimg = image.getScaledInstance(52, 15, Image.SCALE_AREA_AVERAGING);
		eyes = new ImageIcon(newimg);
		lblEyes.setText("");
		lblEyes.setIcon(eyes);
		lblEyes.setLocation(273, 24);

		String clothsb = "cloth\\cloth" + avatar[3] + ".png";
		ImageIcon cloth = new ImageIcon(clothsb);
		image = cloth.getImage(); // transform it
		newimg = image.getScaledInstance(64, 43, Image.SCALE_AREA_AVERAGING);
		cloth = new ImageIcon(newimg);
		lblCloth.setText("");
		lblCloth.setIcon(cloth);
		lblCloth.setLocation(267, 47);

		String accsb = "acc\\acc" + avatar[4] + ".png";
		ImageIcon acc = new ImageIcon(accsb);
		image = acc.getImage(); // transform it
		newimg = image.getScaledInstance(35, 25, Image.SCALE_AREA_AVERAGING);
		acc = new ImageIcon(newimg);
		lblAcc.setText("");
		lblAcc.setIcon(acc);
		lblAcc.setLocation(325, 72);

	}
}
