package CottonCandy;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class AvartarEdit extends JFrame implements Runnable, ActionListener {

	private JPanel contentPane;

	private JLabel lblskinprew;
	private JLabel lblhairprew;
	private JLabel lbleyesprew;
	private JLabel lblclothprew;
	private JLabel lblaccprew;

	private int skinnum;
	private int hairnum;
	private int eyesnum;
	private int clothnum;
	private int accnum;

	private JComboBox CBskin;
	private JComboBox CBhair;
	private JComboBox CBeyes;
	private JComboBox CBcloth;
	private JComboBox CBacc;
	private JButton btnreset;
	private JButton btnsave;
	private JButton btnbasic;
	
	private Join join;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AvartarEdit frame = new AvartarEdit();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws Exception 
	 */
	public AvartarEdit(Join join) throws Exception {
		this();
		this.join=join;
	}
	
	public AvartarEdit() throws Exception {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 800, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblskin = new JLabel("피부색");
		lblskin.setFont(new Font("맑은 고딕", Font.BOLD, 20));
		lblskin.setBounds(408, 12, 110, 49);

		lblaccprew = new JLabel("");
		lblaccprew.setBounds(14, 12, 380, 529);

		contentPane.add(lblaccprew);
		contentPane.add(lblaccprew);

		lblclothprew = new JLabel("");
		lblclothprew.setBounds(14, 12, 380, 529);

		contentPane.add(lblclothprew);

		lbleyesprew = new JLabel("");
		lbleyesprew.setBounds(14, 12, 380, 529);

		contentPane.add(lbleyesprew);

		lblhairprew = new JLabel("");
		lblhairprew.setBounds(14, 12, 380, 529);

		contentPane.add(lblhairprew);

		lblskinprew = new JLabel("");
		lblskinprew.setBounds(14, 12, 380, 529);

		contentPane.add(lblskinprew);

		CBskin = new JComboBox();
		CBskin.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					skinnum = CBskin.getSelectedIndex();

					String skinsb = "skin\\skin" + skinnum
							+ ".png";
					System.out.println(skinnum);
					System.out.println(skinsb);
					// String basic = "C:\\Users\\ssin7\\Desktop\\자바
					// 교육\\프로젝트\\아바타 제작\\avatar\\basic\\nothing.png";
					try {

						ImageIcon skin = new ImageIcon(skinsb);
						Image image = skin.getImage(); // transform it
						Image newimg = image.getScaledInstance(200, 200, Image.SCALE_AREA_AVERAGING);

						skin = new ImageIcon(newimg);
						lblskinprew.setIcon(skin);
						// 중앙정렬
						lblskinprew.setHorizontalAlignment(JLabel.CENTER);
						lblskinprew.setVerticalAlignment(JLabel.CENTER);
						System.out.println(lblskinprew.getVerticalTextPosition());
						System.out.println(lblskinprew.getHorizontalTextPosition());
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(AvartarEdit.this, "파일이 없습니다.");
					}
					// } catch (IOException e1) {
					// e.getStateChange();
					// }
				}

			}
		});

		CBskin.setModel(
				new DefaultComboBoxModel(new String[] { "--------선택하세요--------", "1. 살구색", "2. ㅇㅇ색", "3. ㅇㅇ색" }));
		CBskin.setBounds(540, 28, 228, 24);
		contentPane.add(CBskin);

		CBhair = new JComboBox();
		CBhair.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					hairnum = CBhair.getSelectedIndex();

					String hairsb = "hair\\hair" + hairnum
							+ ".png";
					System.out.println(hairnum);
					System.out.println(hairsb);
					// String basic = "C:\\Users\\ssin7\\Desktop\\자바
					// 교육\\프로젝트\\아바타 제작\\avatar\\basic\\nothing.png";
					try {

						ImageIcon hair = new ImageIcon(hairsb);
						Image image = hair.getImage(); // transform it
						Image newimg = image.getScaledInstance(180, 80, Image.SCALE_AREA_AVERAGING);

						hair = new ImageIcon(newimg);
						lblhairprew.setIcon(hair);
						// 중앙정렬
						lblhairprew.setLocation(110, -65);

						// lblhairprew.setVerticalAlignment(JLabel.CENTER);
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(AvartarEdit.this, "파일이 없습니다.");
					}
					// } catch (IOException e1) {
					// e.getStateChange();
					// }
				}

			}
		});
		contentPane.setVisible(true);

		CBhair.setModel(
				new DefaultComboBoxModel(new String[] { "--------선택하세요--------", "1. 바가지머리", "2. ㅇㅇㅇ머리", "3. ㅇㅇㅇ머리" }));
		CBhair.setBounds(540, 89, 228, 24);
		contentPane.add(CBhair);

		CBeyes = new JComboBox();
		CBeyes.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					eyesnum = CBeyes.getSelectedIndex();

					String eyessb = "eyes\\eyes" + eyesnum
							+ ".png";
					System.out.println(eyesnum);
					System.out.println(eyessb);
					// String basic = "C:\\Users\\ssin7\\Desktop\\자바
					// 교육\\프로젝트\\아바타 제작\\avatar\\basic\\nothing.png";
					try {

						ImageIcon eyes = new ImageIcon(eyessb);
						Image image = eyes.getImage(); // transform it
						Image newimg = image.getScaledInstance(115, 35, Image.SCALE_AREA_AVERAGING);

						eyes = new ImageIcon(newimg);
						lbleyesprew.setIcon(eyes);
						// 중앙정렬
						lbleyesprew.setLocation(146, -2);

						// lblhairprew.setVerticalAlignment(JLabel.CENTER);
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(AvartarEdit.this, "파일이 없습니다.");
					}
					// } catch (IOException e1) {
					// e.getStateChange();
					// }
				}

			}
		});
		contentPane.setVisible(true);
		CBeyes.setModel(
				new DefaultComboBoxModel(new String[] { "--------선택하세요--------", "1. 찢어진 눈", "2. ㅇㅇㅇ 눈", "3. ㅇㅇㅇ 눈" }));
		CBeyes.setBounds(540, 150, 228, 24);
		contentPane.add(CBeyes);

		CBcloth = new JComboBox();
		CBcloth.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					clothnum = CBcloth.getSelectedIndex();

					String clothsb = "cloth\\cloth" + clothnum
							+ ".png";
					System.out.println(clothnum);
					System.out.println(clothsb);
					// String basic = "C:\\Users\\ssin7\\Desktop\\자바
					// 교육\\프로젝트\\아바타 제작\\avatar\\basic\\nothing.png";
					try {

						ImageIcon cloth = new ImageIcon(clothsb);
						Image image = cloth.getImage(); // transform it
						Image newimg = image.getScaledInstance(138, 95, Image.SCALE_AREA_AVERAGING);

						cloth = new ImageIcon(newimg);
						lblclothprew.setIcon(cloth);
						// 중앙정렬
						lblclothprew.setLocation(134, 48);

						// lblhairprew.setVerticalAlignment(JLabel.CENTER);
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(AvartarEdit.this, "파일이 없습니다.");
					}
					// } catch (IOException e1) {
					// e.getStateChange();
					// }
				}

			}
		});
		contentPane.setVisible(true);
		CBcloth.setModel(new DefaultComboBoxModel(
				new String[] { "--------선택하세요--------", "1. 검은패딩 ", "2. ㅇㅇ패딩", "3. ㅇㅇ패딩", "" }));
		CBcloth.setBounds(540, 211, 228, 24);
		contentPane.add(CBcloth);

		CBacc = new JComboBox();
		CBacc.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					accnum = CBacc.getSelectedIndex();

					String accsb = "acc\\acc" + accnum + ".png";
					System.out.println(accnum);
					System.out.println(accsb);
					// String basic = "C:\\Users\\ssin7\\Desktop\\자바
					// 교육\\프로젝트\\아바타 제작\\avatar\\basic\\nothing.png";
					try {

						ImageIcon acc = new ImageIcon(accsb);
						Image image = acc.getImage(); // transform it
						Image newimg = image.getScaledInstance(80, 60, Image.SCALE_AREA_AVERAGING);

						acc = new ImageIcon(newimg);
						lblaccprew.setIcon(acc);
						// 중앙정렬
						lblaccprew.setLocation(250, 95);

						// lblhairprew.setVerticalAlignment(JLabel.CENTER);
					} catch (Exception e1) {
						JOptionPane.showMessageDialog(AvartarEdit.this, "파일이 없습니다.");
					}
					// } catch (IOException e1) {
					// e.getStateChange();
					// }
				}

			}
		});
		contentPane.setVisible(true);
		CBacc.setModel(
				new DefaultComboBoxModel(new String[] { "--------선택하세요--------", "1. 고양이", "2. ㅇㅇㅇ", "3. ㅇㅇㅇ" }));
		CBacc.setBounds(540, 280, 228, 24);
		contentPane.add(CBacc);

		btnreset = new JButton("초기화");
		btnreset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String basic = "basic\\nothing.png";
				System.out.println(basic);

				// String basic = "C:\\Users\\ssin7\\Desktop\\자바
				// 교육\\프로젝트\\아바타 제작\\avatar\\basic\\nothing.png";
				File file = new File(basic);

				try {

					ImageIcon nothing = new ImageIcon(basic);
					Image image = nothing.getImage(); // transform it
					Image newimg = image.getScaledInstance(200, 200, Image.SCALE_AREA_AVERAGING);

					nothing = new ImageIcon(newimg);
					lblskinprew.setIcon(nothing);
					lblhairprew.setIcon(null);
					lbleyesprew.setIcon(null);
					lblclothprew.setIcon(null);
					lblaccprew.setIcon(null);
					// 중앙정렬
					lblskinprew.setHorizontalAlignment(JLabel.CENTER);
					lblskinprew.setVerticalAlignment(JLabel.CENTER);
					System.out.println(lblskinprew.getVerticalTextPosition());
					System.out.println(lblskinprew.getHorizontalTextPosition());
					
					CBskin.setSelectedIndex(0);
					CBhair.setSelectedIndex(0);
					CBeyes.setSelectedIndex(0);
					CBcloth.setSelectedIndex(0);
					CBacc.setSelectedIndex(0);
					
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(AvartarEdit.this, "파일이 없습니다.");
				}

			}
		});
		btnreset.setBounds(408, 325, 360, 49);
		contentPane.add(btnreset);

		btnsave = new JButton("저장하기");
		btnsave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int[] avatar={CBskin.getSelectedIndex(),CBhair.getSelectedIndex(),
						CBeyes.getSelectedIndex(),CBcloth.getSelectedIndex(),CBacc.getSelectedIndex()};
				
				join.Avatar(avatar);
				dispose();
			}
		});
		btnsave.setBounds(408, 447, 360, 49);
		contentPane.add(btnsave);

		btnbasic = new JButton("기본으로 설정하기");
		btnbasic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnbasic.setBounds(408, 386, 360, 49);
		contentPane.add(btnbasic);

		JLabel subskin = new JLabel("몸 색깔");
		subskin.setFont(new Font("맑은 고딕", Font.BOLD, 20));
		subskin.setBounds(408, 12, 110, 49);
		contentPane.add(subskin);
		JLabel subhair = new JLabel("머리 모양");
		subhair.setFont(new Font("맑은 고딕", Font.BOLD, 20));
		subhair.setBounds(408, 73, 110, 49);
		contentPane.add(subhair);
		JLabel subeyes = new JLabel("눈 모양");
		subeyes.setFont(new Font("맑은 고딕", Font.BOLD, 20));
		subeyes.setBounds(408, 134, 110, 49);
		contentPane.add(subeyes);
		JLabel subcloth = new JLabel("옷 종류");
		subcloth.setFont(new Font("맑은 고딕", Font.BOLD, 20));
		subcloth.setBounds(408, 195, 110, 49);
		contentPane.add(subcloth);
		JLabel subacc = new JLabel("악세서리");
		subacc.setFont(new Font("맑은 고딕", Font.BOLD, 20));
		subacc.setBounds(408, 264, 110, 49);
		contentPane.add(subacc);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}
}
