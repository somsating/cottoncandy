package CottonCandy;

import java.sql.Date;

public class DTO2 {

	private int idx;
	private Date joindate;
	private int block;
	private Date blockdate;

	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public Date getJoindate() {
		return joindate;
	}

	public void setJoindate(Date joindate) {
		this.joindate = joindate;
	}

	public int getBlock() {
		return block;
	}

	public void setBlock(int block) {
		this.block = block;
	}

	public Date getBlockdate() {
		return blockdate;
	}

	public void setBlockdate(Date blockdate) {
		this.blockdate = blockdate;
	}

	@Override
	public String toString() {
		return "DTO2 [idx=" + idx + ", joindate=" + joindate + ", block=" + block + ", blockdate=" + blockdate + "]";
	}

	public DTO2() {

	}

	public DTO2(int idx, Date joindate, int block, Date blockdate) {
		super();
		this.idx = idx;
		this.joindate = joindate;
		this.block = block;
		this.blockdate = blockdate;
	}

	public int getidx() {
		// TODO Auto-generated method stub
		return 0;
	}



}
